#!/bin/bash



#Removes all network preference .plist files. This can resolve network connection issues.



rm -Rf /Library/Preferences/SystemConfiguration/com.apple.network.eapolclient.configuration.plist

rm -Rf /Library/Preferences/SystemConfiguration/com.apple.airport.preferences.plist

rm -Rf /Library/Preferences/SystemConfiguration/com.apple.airport.preferences.plist-new

rm -Rf /Library/Preferences/SystemConfiguration/NetworkInterfaces.plist

rm -Rf /Library/Preferences/SystemConfiguration/com.apple.wifi.message-tracer.plist
